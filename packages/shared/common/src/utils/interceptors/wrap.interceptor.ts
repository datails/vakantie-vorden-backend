import {
  Injectable,
  NestInterceptor,
  ExecutionContext,
  CallHandler,
} from "@nestjs/common";
import { Observable } from "rxjs";
import { map } from "rxjs/operators";

interface Response<T> {
  data: T;
}

@Injectable()
export class WrapInterceptor<T> implements NestInterceptor<T, Response<T>> {
  intercept(
    _context: ExecutionContext,
    next: CallHandler
  ): Observable<Response<T>> {
    return next.handle().pipe(
      map((data) => {
        // services with find all already return
        // a data object
        if (data?.data && data?.total) {
          return data;
        }

        if (Array.isArray(data)) {
          return data;
        }

        // for other services,
        // add the data wrapper
        return {
          data: {
            ...data,
            id: data?._id,
          },
        };
      })
    );
  }
}
