import { FilesInterceptor, FileInterceptor } from "@nestjs/platform-express";

import { imageFileFilter } from "./utils";

export const filesInterceptor = FilesInterceptor("files", 30, {
  fileFilter: imageFileFilter,
});

export const fileInterceptor = FileInterceptor("file", {
  fileFilter: imageFileFilter, 
})