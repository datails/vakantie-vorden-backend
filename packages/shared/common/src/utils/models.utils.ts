export interface Resize {
    size: number;
    title: string;
}