import * as R from "ramda";
import { Model, Query, QueryOptions, Types } from "mongoose";

import * as D from "@shared/contracts";
import * as U from "./utils";

export interface CreateDto {
  files?: any[];
  file?: any;
}

export const getTotal = R.curryN(3, (
  model: Query<any, any>,
  portalId?: string,
  isSuperUser?: boolean
) => {
  if (!portalId?.toString?.() || isSuperUser) {
    return model.countDocuments();
  }

  return model.where("portalId").equals(portalId).countDocuments();
})

export const create = R.curryN(
  3,
  async <T extends CreateDto>(
    model: Model<any>,
    createDto: T,
    options: any,
    portalId: string
  ) => {
    if (Object.keys(options).length) {
      return (await model.create([{
        ...createDto,
        ...await U.handleResize(createDto),
        portalId,
      }], options)).find(Boolean)?.toJSON();
    }

    return (await model.create({
      ...createDto,
      ...await U.handleResize(createDto),
      portalId,
    }))?.toJSON();
  }
);

export const findAll = R.curryN(
  4,
  async (
    model: Model<any>,
    findAllDto: D.FindAllDto,
    portalId: string,
    isSuperUser: boolean
  ) => {
    const total = await getTotal(model.find(), portalId, isSuperUser);

    const range = findAllDto.range || [0, total];
    const filter = findAllDto.filter || {};
    const sort = findAllDto.sort
      ? {
          [findAllDto.sort[0]]:
          findAllDto.sort[1] === "ASC" ? 1 : -1,
        }
      : { $natural: -1 };

    const baseQuery = model.find();

    for (const flt in filter) {
      if (flt === "id") {
        const filters = filter.id.map((filter: string) =>
          Types.ObjectId(filter)
        );
        baseQuery.where("_id").in(filters);
      } else {
        if (Array.isArray(model[flt])) {
          baseQuery.where(flt).in(filter[flt]);
        } else {
          baseQuery.where(flt).equals(filter[flt]);
        }
      }
    }

    if (portalId && !isSuperUser) {
      baseQuery.where("portalId").equals(portalId);
    }

    const data = await baseQuery
      .sort(sort)
      .skip(range[0])
      .limit(range[1])
      .lean()
      .exec();

    return {
      data,
      total,
    };
  }
);

export const findByDate = R.curryN(
  4,
  async (
    model: Model<any>,
    findByDateDto: D.FindAllByDateDto,
    portalId: string,
    isSuperUser: boolean
  ) => {
    const total = await getTotal(model.find(), portalId, isSuperUser);

    const range = findByDateDto.range || [0, total];

    const baseQuery = model.find({
      property: {
        $eq: findByDateDto.filter.property,
      },
      startDate: {
        $lt: findByDateDto.filter.endDate,
      },
      endDate: {
        $gt: findByDateDto.filter.startDate,
      },
    });

    if (portalId && !isSuperUser) {
      baseQuery.where("portalId").equals(portalId);
    }

    const data = await baseQuery
      .sort({
        [findByDateDto.sort[0]]: findByDateDto.sort[1] === "ASC" ? 1 : -1,
      })
      .skip(range[0])
      .limit(range[1])
      .lean()
      .exec();

    return {
      data,
      total,
    };
  }
);

export const findById = async (model: Model<any>, id: string) => {
  return model.findById(id).lean().exec()
};

export const findOne = async (
  model: Model<any>,
  query: Record<string, string>,
  options?: QueryOptions,
) => {
  return model.findOne(query, null, options).lean().exec();
};

export const putMany = R.curryN(
  4,
  async <T extends CreateDto>(
    model: Model<any>,
    updateDto: T,
    id: string,
    portalId: string
  ) => {
    return model
      .updateMany(
        {
          _id: id,
        },
        {
          ...updateDto,
          ...await U.handleResize(updateDto),
          portalId,
        }
      )
      .lean()
      .exec();
  }
);

export const putOne = R.curryN(
  4,
  async <T extends CreateDto>(
    model: Model<any>,
    createDto: T,
    id: string,
    portalId: string
  ) => {
    const currentItem = await findById(model, id);

    if (!createDto.files || createDto.files.length === 0) {
      delete createDto.files;
    }

    if (!createDto.file) {
      delete createDto.file;
    }
    
    return model
        .findOneAndUpdate(
          {
            _id: id,
          },
          {
            ...currentItem,
            ...createDto,
            ...await U.handleResize(createDto),
            portalId,
          },
          {
            new: true,
          }
        )
        .lean()
        .exec()
  }
);

export const deleteMany = R.curryN(
  2,
  async (model: Model<any>, deleteDto: D.DeleteDto) => {
    await model
      .deleteMany({
        _id: deleteDto.filter,
      })
      .lean()
      .exec();

    return deleteDto.filter;
  }
);

export const deleteOne = R.curryN(2, async (model: Model<any>, id: string) => {
  return (
    await model
      .findOneAndDelete({
        _id: id,
      })
      .lean()
      .exec()
  )
});
