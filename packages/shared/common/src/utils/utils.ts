import * as P from "path";
import { v4 } from "uuid";
import { Request } from "express";
import { UploadApiErrorResponse, UploadApiResponse, v2 } from 'cloudinary';
import { defaultSizes, extendedSizes } from './constant.utils'

const toStream = require('buffer-to-stream');

v2.config({
  cloud_name: process.env.CLOUDINARY_CLOUD_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_SECRET,
})

export const uploadToCloudinary = (args: { file: Express.Multer.File, size: number, name: string, title: string, ext: string }): Promise<UploadApiResponse | UploadApiErrorResponse> => {
  return new Promise((resolve, reject) => {
    const upload = v2.uploader.upload_stream({
      access_mode: 'public',
      transformation: [{ width: args.size }],
      filename_override: `${args.name}-${args.title}${args.ext}`,
      folder: process.env.CLOUDINARY_FOLDER,
      unique_filename: false,
      use_filename: true,
    }, (error, result) => {
      if (error) return reject(error);
      resolve(result);
    });

    toStream(args.file.buffer).pipe(upload);
  });
}

export const handleResize = async <T extends Record<string,any>>(dto: T) => {
  if (dto.files && Array.isArray(dto.files)) {
    return {
      files: await Promise.all(
        dto.files.map(async (file) => resizeImages(file, extendedSizes)).filter(Boolean)
      )
    }
  }

  if (dto.file && Object.keys(dto.file).length) {
    return { file: resizeImages(dto.file) }
  }

  return {}
}

export const areFileNamesDefined = (file: Express.Multer.File & { fileNames?: string[]}) =>
  Array.isArray(file.fileNames) && file.fileNames.length > 0

export const getExistingFileName = (file: Express.Multer.File & { fileNames?: string[] }) => {
  const fileName = file.fileNames.find(Boolean)
  const [, ...rest] = fileName.split(/\s*\-\s*/g).reverse()
  return rest.join('')
}

export const createFileName = (file: Express.Multer.File & { fileNames?: string[] }) => {
  if (!areFileNamesDefined(file)) {
    const [name] = file.originalname?.split?.(".");
    return `${v4()}-${name}`
  }

  return getExistingFileName(file)
}

export const resizeImages = async (file: Express.Multer.File, sizes = defaultSizes) => {
  if (!file) {
    return;
  }

  // prevent storing the buffer
  const { buffer, ...newFile } = file
  const ext = P.extname(newFile.originalname);

  const newFileName = createFileName(file)

  if (file.buffer) {
    await Promise.all(
      sizes.map(async ({ size, title }) => uploadToCloudinary({ file, name: `${newFileName}`, title, size, ext })).filter(Boolean)
    );
  }

  return {
    ...newFile,
    fileNames: file.buffer ? sizes.map(({ title }) => `${newFileName}-${title}${ext}`).filter(Boolean) : (file as any).fileNames
  }
};

export function getFilePath(files: Array<Express.Multer.File>) {
  return files.map((file) => {
    const fileName = ((file as any).fileNames || []).find(file => file.includes('-xs'));
    return `${process.env.CLOUDINARY_BASE_URL}/${fileName}`;
  });
}

export const imageFileFilter = (_req: Request, file, callback: Function) => {
  if (!file.originalname.match(/\.(jpg|jpeg|png|gif|webp)$/)) {
    return callback(new Error("Only image files are allowed!"), false);
  }
  callback(null, true);
};

export const editFileName = (_req: Request, file, callback: Function) => {
  const fileExtName = P.extname(file.originalname);
  const randomName = v4();

  callback(null, `${randomName}${fileExtName}`);
};

export function getAccessTokenFromRequest(req: any = {}): string {
  return getToken(req.req || req);
}

export function getToken(request: any) {
  return (
    getTokenFromQuery(request) ||
    getTokenFromBearer(request) ||
    getTokenFromBody(request) ||
    ""
  );
}

export function getTokenFromQuery(request: any) {
  return request.query?.access_token;
}

export function getTokenFromBearer(request: any) {
  return request?.headers?.authorization?.split?.("Bearer ")?.[1];
}

export function getTokenFromBody(request: any) {
  request?.body?.access_token;
}

export const getApiHeader = (request: any) => {
  return request?.headers?.['x-portal-id']
}