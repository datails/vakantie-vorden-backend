import * as SW from '@nestjs/swagger';
import * as V from "class-validator";
import { Transform } from "class-transformer";

import { toJSON } from './dto.utils'

export class FilterByDate {
  @SW.ApiProperty({ type: Date, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly startDate: Date;

  @SW.ApiProperty({ type: Date, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly endDate: Date;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly property: string;
}

export class FindAllDto {
  @SW.ApiProperty({ type: [Number], required: false })
  @V.IsOptional()
  @V.IsInt({ each: true })
  @Transform(toJSON)
  readonly range: number[];

  @SW.ApiProperty({ type: [String], required: false })
  @V.IsOptional()
  @V.IsString({ each: true })
  @Transform(toJSON)
  readonly sort: string[];

  @SW.ApiProperty({ type: Object, required: false })
  @V.IsOptional()
  @Transform(toJSON)
  readonly filter: Record<any, any>;
}

export class FindAllByDateDto {
  @SW.ApiProperty({ type: [Number], required: false })
  @V.IsOptional()
  @V.IsInt({ each: true })
  @Transform(toJSON)
  readonly range: number[];

  @SW.ApiProperty({ type: [String], required: false })
  @V.IsOptional()
  @V.IsString({ each: true })
  @Transform(toJSON)
  readonly sort: string[];

  @SW.ApiProperty({ type: Object, required: true })
  @V.IsNotEmpty()
  @Transform(toJSON)
  readonly filter: FilterByDate;
}

export class DeleteDto {
  @SW.ApiProperty({ type: [String], required: true })
  @V.IsOptional()
  @V.IsString({ each: true })
  @Transform(toJSON)
  readonly filter: string[];
}
