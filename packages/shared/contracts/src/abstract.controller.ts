import * as N from "@nestjs/common";

import { Role, Roles } from "@module/auth-module";
import { AbstractService } from "./abstract.service";
import { FindAllDto, DeleteDto } from "./dto.contract";

export abstract class AbstractController<T extends AbstractService<U, Y>, U, Y> {
  constructor(private readonly service: T) {}

  @N.Post()
  @N.Header("Content-Type", "application/json")
  @Roles(Role.Admin)
  async create(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createDto: U
  ) {
    return this.service.create(createDto);
  }

  @N.Get("/many")
  @N.Header("Content-Type", "application/json")
  @Roles(Role.Admin)
  async findAll(
    @N.Query(new N.ValidationPipe({ transform: true }))
    findAllDto: FindAllDto
  ): Promise<{ data: Y[]; total: number }> {
    return this.service.findAll(findAllDto);
  }

  @N.Get("/:id")
  @N.Header("Content-Type", "application/json")
  @Roles(Role.Admin, Role.User)
  async findById(@N.Param() { id }): Promise<Y> {
    return this.service.findById(id);
  }

  @N.Put("/:id")
  @N.Header("Content-Type", "application/json")
  @Roles(Role.Admin)
  async putOne(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createDto: U,
    @N.Param() { id }
  ): Promise<Y> {
    return this.service.putOne(id, createDto);
  }

  @N.Put("/many/:id")
  @N.Header("Content-Type", "application/json")
  @Roles(Role.Admin)
  async putMany(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createClientDto: U,
    @N.Param() { id }
  ): Promise<Y> {
    return this.service.putMany(id, createClientDto);
  }

  @N.Delete("/:id")
  @N.Header("Content-Type", "application/json")
  @Roles(Role.Admin)
  async deleteOne(@N.Param() { id }): Promise<Y> {
    return this.service.deleteOne(id);
  }

  @N.Delete("/many/:id")
  @N.Header("Content-Type", "application/json")
  @Roles(Role.Admin)
  async deleteMany(
    @N.Query(new N.ValidationPipe({ transform: true }))
    deleteDto: DeleteDto
  ): Promise<string[]> {
    return this.service.deleteMany(deleteDto);
  }
}
