import { TransformFnParams } from 'class-transformer'

export function toObject({ value }: TransformFnParams) {
    return JSON.parse(value) || {};
  }
  
  export function toJSON({ value }: TransformFnParams) {
    return JSON.parse(value);
  }
  
  export function toArray({ value }: TransformFnParams) {
    return value.split(",");
  }
  
  export function toFile({ value }: TransformFnParams) {
    return JSON.parse(value) || {};
  }
  
  export function toBoolean({ value }: TransformFnParams) {
    return value === "true";
  }
  
  export function toParsedArray({ value }: TransformFnParams) {
    return value?.map?.((item) => JSON.parse(item)) || [];
  }
  
  export function toNumber({ value }: TransformFnParams) {
    return parseInt(value, 10);
  }