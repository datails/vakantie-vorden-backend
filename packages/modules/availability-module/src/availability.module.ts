import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";

import { PropertyModule } from "@module/property-module";
import { APP_MONGO_DB } from "@module/mongo-module";

import { ReservationService } from "./availability/availability.service";
import { AvailabilityController }from "./availability/availability.controller";
import { ReservationController } from "./reservation/reservation.controller"
import * as S from "./availability/availability.schema";

/**
 * due to a bug that we cannot use scoped requests
 * and forwardRef 2 ways, we had to merge the availability
 * and reservation services. Therefor the reservation service
 * contains all the availability services as well
 */

@Module({
  imports: [
    PropertyModule,
    MongooseModule.forFeature(
      [
        { name: S.Availability.name, schema: S.AvailabilitySchema },
        { name: S.Reservation.name, schema: S.ReservationSchema },
      ],
      APP_MONGO_DB
    ),
  ],
  controllers: [AvailabilityController, ReservationController],
  providers: [ReservationService],
  exports: [ReservationService],
})
export class ReservationModule {}
