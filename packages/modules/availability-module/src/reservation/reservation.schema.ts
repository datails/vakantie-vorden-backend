import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import * as S from '@nestjs/swagger';
import * as mongoose from "mongoose";

export type ReservationDocument = Reservation & mongoose.Document;

@Schema({
  autoCreate: true,
  id: true,
  timestamps: true,
})
export class Reservation {
  @S.ApiProperty({ type: String, required: true })
  @Prop({ type: String, required: true })
  portalId: string;

  @S.ApiProperty({ type: String, required: true })
  @Prop({ type: String, required: true })
  client: string;

  @S.ApiProperty({ type: String, required: true })
  @Prop({ type: String, required: true })
  property: string;

  @S.ApiProperty({ type: Number, required: true })
  @Prop({ type: Number, required: true })
  price: Number;

  @S.ApiProperty({ type: Number, required: true })
  @Prop({ type: Number, required: true })
  adults: number;

  @S.ApiProperty({ type: Number, required: false })
  @Prop({ type: Number, required: false })
  children: number;

  @S.ApiProperty({ type: Date, required: true })
  @Prop({ type: Date, required: true })
  startDate: Date;

  @S.ApiProperty({ type: Date, required: true })
  @Prop({ type: Date, required: true })
  endDate: Date;

  @S.ApiProperty({ type: String, required: true })
  @Prop({ type: String, required: true })
  status: string;

  @S.ApiProperty({ type: Boolean, required: true })
  @Prop({ type: Boolean, required: true })
  isPaid: boolean;
}

export const ReservationSchema = SchemaFactory.createForClass(Reservation);
