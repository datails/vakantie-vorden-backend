import * as N from "@nestjs/common";
import * as SW from '@nestjs/swagger';

import * as C from "@shared/contracts";

import { ReservationService } from "../availability/availability.service";
import { Reservation } from "./reservation.schema";

import * as D from "./reservation.dto";

@SW.ApiTags("Reservation")
@N.Controller("reservations")
export class ReservationController extends C.AbstractController<
  ReservationService,
  D.CreateReservationDto,
  Reservation
> {
  constructor(reservationService: ReservationService) {
    super(reservationService);
  }
}
