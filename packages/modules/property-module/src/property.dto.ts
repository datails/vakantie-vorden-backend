import * as SW from '@nestjs/swagger';
import * as V from "class-validator";
import { Transform } from "class-transformer";
import * as C from "@shared/contracts";

export class CreatePropertyDto {
  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly title: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly description: string;

  @SW.ApiProperty({ type: [Object], required: false })
  @V.IsOptional()
  @Transform(C.toParsedArray)
  files: Array<Record<any, any>>;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly rules: string;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsInt()
  @Transform(C.toNumber)
  readonly price: number;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsInt()
  @Transform(C.toNumber)
  readonly minNights: number;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsInt()
  @Transform(C.toNumber)
  readonly servicePrice: number;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly isVATIncluded: boolean;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsInt()
  @Transform(C.toNumber)
  readonly numberOfDaysBeforeCancel: number;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsInt()
  @Transform(C.toNumber)
  readonly persons: number;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsInt()
  @Transform(C.toNumber)
  readonly squareMeters: number;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsInt()
  @Transform(C.toNumber)
  readonly chambers: number;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsInt()
  @Transform(C.toNumber)
  readonly beds: number;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsInt()
  @Transform(C.toNumber)
  readonly bathrooms: number;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly checkin: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly checkout: string;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsNumberString()
  readonly lat: number;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsNumberString()
  readonly lng: number;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly city: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly street: string;

  @SW.ApiProperty({ type: Number, required: true })
  @V.IsNotEmpty()
  @V.IsInt()
  @Transform(C.toNumber)
  readonly housenumber: number;

  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsString()
  readonly housenumber_extension?: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsPostalCode("NL")
  readonly zipcode: string;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly isActive: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly isHandicapped: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly isSmokingProperty: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly isFitForBabies: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly isFitForAnimals: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasTelevision: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasWarmWater: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasHeating: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasFireplace: boolean;

  @SW.ApiProperty({ type: Object, required: false })
  @V.IsOptional()
  readonly basicServices: Record<any, any>;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasWifi: boolean;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString({ each: true })
  readonly parking: string[];

  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasStove: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasPlatesAndCutlery: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasMicrowave: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasCoffeeMachine: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasCookingSupplies: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasKitchen: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasFridge: boolean;

  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasOwnEntrance: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly isCleaningBeforeLeave: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasClothesHangers: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasBedding: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasShowerGel: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasShampoo: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasGarden: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasBarn: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasSmokeAlarm: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasCarbonMonoxideDetector: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasHairDryer: boolean;

  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasAirconditioning: boolean;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  @Transform(C.toBoolean)
  readonly hasWachingMachine: boolean;
}
