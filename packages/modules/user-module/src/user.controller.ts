import * as N from "@nestjs/common";
import * as SW from '@nestjs/swagger';

import * as C from "@shared/contracts";
import * as U from "@module/common";
import * as A from "@module/auth-module";

import { UsersService } from "./user.service";
import { CreateUserDTO } from "./user.dto";
import { User } from "./user.schema";

@SW.ApiTags("User")
@N.Controller("users")
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @N.Post()
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  @N.UseInterceptors(U.fileInterceptor)
  async create(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createUserDto: CreateUserDTO,
    @N.UploadedFile()
    file: Record<any, any>
  ) {
    return this.usersService.create({
      ...createUserDto,
      file,
    });
  }

  @N.Get("/many")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  async findAll(
    @N.Query(new N.ValidationPipe({ transform: true }))
    findAllUsersDto: C.FindAllDto
  ): Promise<{ data: User[]; total: number }> {
    return this.usersService.findAll(findAllUsersDto);
  }

  @N.Get("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin, A.Role.User)
  async findById(@N.Param() { id }): Promise<User> {
    return this.usersService.findById(id);
  }

  @N.Put("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  @N.UseInterceptors(U.fileInterceptor)
  async putOne(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createUserDto: CreateUserDTO,
    @N.Param() { id },
    @N.UploadedFile() file: Record<any, any>
  ): Promise<User> {
    return this.usersService.putOne(id, {
      ...createUserDto,
      file,
    });
  }

  @N.Put("/many/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  @N.UseInterceptors(U.fileInterceptor)
  async putMany(
    @N.Body(new N.ValidationPipe({ transform: true }))
    createUserDto: CreateUserDTO,
    @N.Param() { id },
    @N.UploadedFile() file: Record<any, any>
  ): Promise<User> {
    return this.usersService.putMany(id, {
      ...createUserDto,
      file,
    });
  }

  @N.Delete("/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  async deleteOne(@N.Param() { id }): Promise<User> {
    return this.usersService.deleteOne(id);
  }

  @N.Delete("/many/:id")
  @N.Header("Content-Type", "application/json")
  @A.Roles(A.Role.Admin)
  async deleteMany(
    @N.Query(new N.ValidationPipe({ transform: true }))
    deleteUserDto: C.DeleteDto
  ): Promise<string[]> {
    return this.usersService.deleteMany(deleteUserDto);
  }
}
