import { Module, Global } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { SSO_MONGO_DB } from "@module/mongo-module";

import { UsersController } from "./user.controller";
import { UsersService } from "./user.service";
import { User, UserSchema } from "./user.schema";

@Global()
@Module({
  imports: [
    MongooseModule.forFeature(
      [{ name: User.name, schema: UserSchema }],
      SSO_MONGO_DB
    ),
  ],
  controllers: [UsersController],
  providers: [UsersService],
  exports: [UsersService],
})
export class UsersModule {}
