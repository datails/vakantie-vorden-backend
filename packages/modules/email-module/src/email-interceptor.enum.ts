export const enum EMAIL_TYPES {
  RESERVATION_CREATE = "reservation.create",
  RESERVATION_UPDATE = "reservation.update",
  RESERVATION_DELETE = "reservation.delete",
  RESERVATION_CANCELLED = "reservation.cancelled",
}
