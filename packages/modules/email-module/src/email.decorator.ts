import { SetMetadata } from "@nestjs/common";
import { EMAIL_TYPES } from "./email-interceptor.enum";

export const EMAIL_KEY = "emailType";
export const Email = (...roles: EMAIL_TYPES[]) => SetMetadata(EMAIL_KEY, roles);
