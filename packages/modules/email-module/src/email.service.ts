import { OnEvent } from '@nestjs/event-emitter';
import * as sgMail from "@sendgrid/mail";

import * as P from "@module/public-module";
import * as U from "@module/user-module";
import * as C from "@module/client-module";
import * as R from "@module/availability-module";
import { ConfigService } from '@nestjs/config';
import { Injectable } from '@nestjs/common';

@Injectable()
export class WorkerEmailService {
  constructor(
    private readonly usersService: U.UsersService,
    private readonly configService: ConfigService
  ) {
    sgMail.setApiKey(this.configService.get<string>('app.smtpKey'));
  }

  @OnEvent("reservation.create")
  public async createReservation(data: P.CreatePublicReservationDTO) {
    // @ts-ignore
    const owners = await this.usersService.findAll({
      filter: {
        portalId: data.reservation.portalId,
      },
    }, data.reservation.portalId)

    const promises = owners.data.map(async owner => this.createReservationEmail(
      owner,
      data.client,
      data.reservation
    ))

    await Promise.all(promises)
  }

  private async createReservationEmail(
    owner: U.User,
    client: Partial<C.Client>,
    reservation: Partial<R.Reservation>
  ) {
    const sendgridEmailData = {
      propertyName: reservation.property,
      startDate: reservation.startDate?.toLocaleDateString(),
      endDate: reservation.endDate?.toLocaleDateString(),
      guests: reservation.children + reservation.adults,
      isPaid: reservation.isPaid ? "ja" : "nee",
      price: reservation.price,
    };

    await sgMail.send({
      to: client.email,
      from: "datails.smtp@gmail.com",
      subject: "Bevestiging reserveringsaanvraag",
      templateId: "d-e9390a359805410caa478a8589bfc6d3",
      dynamicTemplateData: {
        title: "Bevestiging reserveringsaanvraag",
        subtitle: "Bedankt voor uw reservering!",
        name: client.firstname,
        // @ts-ignore
        body: `Bedankt voor uw reservering! Uw reserveringsaanvraag met nummer ${reservation._id} moet nog worden bevestigd door de eigenaar. Hiervan ontvangt u zo spoedig mogelijk bericht.`,
        ...sendgridEmailData,
      },
    });

    await sgMail.send({
      to: owner.email,
      from: "datails.smtp@gmail.com",
      subject: "Nieuwe reserveringsaanvraag",
      templateId: "d-e9390a359805410caa478a8589bfc6d3",
      dynamicTemplateData: {
        title: "Nieuwe reserveringsaanvraag",
        subtitle: "Een nieuwe reservering!",
        name: owner.name,
        // @ts-ignore
        body: `Er is een nieuwe reserveringsaanvraag met nummer ${reservation._id} en deze moet nog door u worden bevestigd.`,
        ...sendgridEmailData,
      },
    });
  }
}
