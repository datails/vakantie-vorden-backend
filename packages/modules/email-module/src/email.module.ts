import { EventEmitterModule } from '@nestjs/event-emitter';
import { Module, Global } from "@nestjs/common";
import { ConfigModule } from '@module/config-module'
import { UsersModule } from '@module/user-module';
import { WorkerEmailService } from './email.service'

@Global()
@Module({
  imports: [
    EventEmitterModule.forRoot(),
    ConfigModule,
    UsersModule,
  ],
  providers: [WorkerEmailService],
})
export class EmailModule {}
