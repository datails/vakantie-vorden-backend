import { Module } from "@nestjs/common";
import * as C from "@nestjs/config";

import appConfig from "./app.config";
import mongoConfig from "./mongo.config";

@Module({
  imports: [
    C.ConfigModule.forRoot({
      isGlobal: true,
      cache: true,
      load: [appConfig, mongoConfig],
    }),
  ],
})
export class ConfigModule {}
