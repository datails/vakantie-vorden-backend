import { registerAs } from "@nestjs/config";

export default registerAs("mongo", () => ({
  uri: process.env.MONGO_URI,
  db: process.env.MONGO_DB_NAME,
}));
