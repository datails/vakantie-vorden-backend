import { registerAs } from "@nestjs/config";

export default registerAs("app", () => ({
  port: process.env.PORT ?? 3000,
  host: process.env.HOST,
  prefix: process.env.PUBLIC_PATH,
  env: process.env.NODE_ENV,
  smtpKey: process.env.SMTP_KEY,
  recaptchaKey: process.env.RECAPTCHA_KEY,
  isProd:
    process.env.NODE_ENV === "prod" || process.env.NODE_ENV === "production",
}));
