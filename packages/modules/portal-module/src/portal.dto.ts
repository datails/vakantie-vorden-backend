import * as SW from '@nestjs/swagger';
import * as V from "class-validator";

export class CreatePortalDto {
  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly name: string;

  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsString()
  @V.IsUrl()
  readonly url?: string;

  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsString()
  readonly ownerId?: string;

  @SW.ApiProperty({ type: String, required: false })
  @V.IsOptional()
  @V.IsIBAN()
  @V.IsString()
  readonly iban?: string;

  @SW.ApiProperty({ type: Boolean, required: false })
  @V.IsOptional()
  @V.IsBoolean()
  readonly isActive: boolean;
}
