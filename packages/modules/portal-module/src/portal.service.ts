import * as N from "@nestjs/common";
import * as M from "@nestjs/mongoose";

import { Model } from "mongoose";

import * as U from "@shared/contracts";

import * as I from "./portal.schema";
import * as D from "./portal.dto";

@N.Injectable()
export class PortalService extends U.AbstractService<
  D.CreatePortalDto,
  I.Portal
> {
  constructor(
    @M.InjectModel(I.Portal.name)
    portalModel: Model<I.PortalDocument>,
  ) {
    super(portalModel);
  }
}
