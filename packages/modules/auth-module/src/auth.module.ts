import { APP_GUARD } from "@nestjs/core";
import { Module, HttpModule } from "@nestjs/common";
import { JwtModule } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { MongooseModule } from "@nestjs/mongoose";

import { AuthService } from "./auth.service";
import { AuthController } from "./auth.controller";
import { jwtConstants } from "./auth.constants";
import { User, UserSchema } from './auth.schema'

import * as S from "./strategies";
import * as G from "./guards";

@Module({
  imports: [
    HttpModule,
    MongooseModule.forFeature(
      [{ name: User.name, schema: UserSchema }],
      "SSO_MONGO_DB"
    ),
    PassportModule,
    JwtModule.register({
      secret: jwtConstants.secret,
      signOptions: { expiresIn: "8h" },
    }),
  ],
  controllers: [AuthController],
  providers: [
    AuthService,
    S.LocalStrategy,
    S.JwtStrategy,
    {
      provide: APP_GUARD,
      useClass: G.JwtAuthGuard,
    },
    {
      provide: APP_GUARD,
      useClass: G.RolesGuard,
    },
  ],
  exports: [AuthService],
})
export class AuthModule {}
