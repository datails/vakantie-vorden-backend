import * as SW from '@nestjs/swagger';
import * as V from "class-validator";
import { Transform } from "class-transformer";

import * as C from "@shared/contracts";

export class CreateLayoutDto {
  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly colorPalette: string;

  @SW.ApiProperty({ type: String, required: true })
  @V.IsNotEmpty()
  @V.IsString()
  readonly portalId: string;

  @SW.ApiProperty({ type: Object, required: false })
  @V.IsOptional()
  @Transform(C.toObject)
  logo: Record<any, any>;
}
