import * as N from "@nestjs/common";
import * as M from "@nestjs/mongoose";

import { Model } from "mongoose";

import * as U from "@shared/contracts";
import * as S from "@module/switch-module";

import * as I from "./layout.schema";
import * as D from "./layout.dto";

@N.Injectable()
export class LayoutService extends U.AbstractService<
  D.CreateLayoutDto,
  I.Layout
> {
  constructor(
    @M.InjectModel(I.Layout.name)
    clientModel: Model<I.LayoutDocument>,
    @N.Inject(S.DYNAMIC_DATABASE_TABLE)
    portal: U.Portal
  ) {
    super(clientModel, portal);
  }
}
