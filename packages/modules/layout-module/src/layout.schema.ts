import { Prop, Schema, SchemaFactory } from "@nestjs/mongoose";
import { Document } from "mongoose";

export type LayoutDocument = Layout & Document;

@Schema({
  autoCreate: true,
  id: true,
  timestamps: true,
})
export class Layout {
  @Prop({ type: String, required: true })
  colorPalette: string;

  @Prop({ type: String, required: true })
  portalId: string;

  @Prop({ type: Buffer, required: true })
  logo: Record<any, any>;
}

export const LayoutSchema = SchemaFactory.createForClass(Layout);
