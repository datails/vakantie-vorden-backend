import * as N from "@nestjs/common";
import * as SW from '@nestjs/swagger';

import * as C from "@shared/contracts";

import * as S from "./layout.service";
import * as I from "./layout.schema";
import * as D from "./layout.dto";

@SW.ApiTags("Layout")
@N.Controller("layouts")
export class ClientController extends C.AbstractController<
  S.LayoutService,
  D.CreateLayoutDto,
  I.Layout
> {
  constructor(layoutService: S.LayoutService) {
    super(layoutService);
  }
}
