import { Module } from "@nestjs/common";
import { MongooseModule } from "@nestjs/mongoose";
import { ConfigService } from "@nestjs/config";
import { APP_MONGO_DB, SSO_MONGO_DB } from "./mongoose.constants";

@Module({
  imports: [
    MongooseModule.forRootAsync({
      connectionName: APP_MONGO_DB,
      // previously this was one with an dynamic
      // mongo db name. But this causes mongo to
      // create new connections over and over again
      // and eventually exausted the resources at mongo's side.
      useFactory: (configService: ConfigService) => ({
        uri: `${configService.get('mongo.uri')}/${configService.get('mongo.db')}`,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        w: "majority",
      }),
      inject: [ConfigService]
    }),
    MongooseModule.forRootAsync({
      connectionName: SSO_MONGO_DB,
      useFactory: (configService: ConfigService) => ({
        uri: `${configService.get('mongo.uri')}/sso`,
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useFindAndModify: false,
        w: "majority",
      }),
      inject: [ConfigService]
    }),
  ],
})
export class MongoModule {}
